// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "MegaCarAttackTamarro.h"
#include "MegaCarAttackTamarroGameMode.h"
#include "MegaCarAttackTamarroPawn.h"
#include "MegaCarAttackTamarroHud.h"
#include "MegaVehicle.h"


AMegaCarAttackTamarroGameMode::AMegaCarAttackTamarroGameMode()
{
	DefaultPawnClass = AMegaCarAttackTamarroPawn::StaticClass();
	HUDClass = AMegaCarAttackTamarroHud::StaticClass();
}
