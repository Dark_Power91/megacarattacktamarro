// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "VehicleWheel.h"
#include "MegaCarAttackTamarroWheelFront.generated.h"

UCLASS()
class UMegaCarAttackTamarroWheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UMegaCarAttackTamarroWheelFront();
};



