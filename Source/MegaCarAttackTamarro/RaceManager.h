// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "RaceManager.generated.h"

UCLASS(BlueprintType, Blueprintable)
class MEGACARATTACKTAMARRO_API ARaceManager : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, Category = ComponentsS)
	UBillboardComponent* Billboard;

	UPROPERTY()
	class AMegaPlayerController* PlayerController;
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Race Settings", meta = (BlueprintProtected = "true"))
	int32 MaxLaps;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Race Settings", meta = (BlueprintProtected = "true"))
	float GoldTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Race Settings", meta = (BlueprintProtected = "true"))
	float SilverTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Race Settings", meta = (BlueprintProtected = "true"))
	float BronzeTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Race Settings", meta = (BlueprintProtected = "true"))
	float DefaultBestRaceTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Race Settings", meta = (BlueprintProtected = "true"))
	float DefaultBestLapTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Settings", meta = (BlueprintProtected = "true"))
	FText MapName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom Settings", meta = (BlueprintProtected = "true"))
	FString SaveGameName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Track")
	TArray<class ACheckpoint*> Checkpoints;

	int32 NumCheckpoints;


public:	
	// Sets default values for this actor's properties
	ARaceManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	
	UFUNCTION()
	void LapCheck(int32 CheckpointPassed);


	UFUNCTION(BlueprintCallable, Category = Checkpoint)
	void SetupPlayerController();
	UFUNCTION(BlueprintCallable, Category = Checkpoint)
	void StartSequence();

	UFUNCTION()
	void RestartGame();

	void LapComplete();
	void PrepareNextCheckpoint(int32 CheckpointPassed);
	void UpdateRespawnLocation(int32 CheckpointPassed);

	void CheckpointsPresentSetup();
	void CheckpointsToRetriveSetup();
};
