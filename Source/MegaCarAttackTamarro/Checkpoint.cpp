// Fill out your copyright notice in the Description page of Project Settings.

#include "MegaCarAttackTamarro.h"
#include "Checkpoint.h"
#include "MegaCarAttackTamarroPawn.h"
#include "Components/ArrowComponent.h"


// Sets default values
ACheckpoint::ACheckpoint()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	ForwardArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	ForwardArrow->SetupAttachment(RootComponent);
	
	PassageEffect = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystem"));
	PassageEffect->SetupAttachment(RootComponent);

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	CollisionBox->SetupAttachment(RootComponent);
	CollisionBox->bGenerateOverlapEvents = true;
	CollisionBox->OnComponentEndOverlap.AddDynamic(this, &ACheckpoint::OnOverlapEnd);

}

// Called when the game starts or when spawned
void ACheckpoint::BeginPlay()
{
	Super::BeginPlay();
	
}

void ACheckpoint::Activate()
{
	PassageEffect->SetHiddenInGame(false);
	CollisionBox->SetHiddenInGame(false);
	CollisionBox->bGenerateOverlapEvents = true;
}

void ACheckpoint::Deactivate()
{
	PassageEffect->SetHiddenInGame(true);
	CollisionBox->SetHiddenInGame(true);
	CollisionBox->bGenerateOverlapEvents = false;
}

int32 ACheckpoint::GetCheckpointIndex()
{
	return CheckPointIndex;
}

void ACheckpoint::SetCheckpointIndex(int32 Index)
{
	CheckPointIndex = Index;
}

void ACheckpoint::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	AMegaCarAttackTamarroPawn* otherPawn = Cast<AMegaCarAttackTamarroPawn>(OtherActor);
	if (otherPawn)
	{
		FVector PawnDirection = otherPawn->GetVelocity();
		FVector ForwardCheckpointDirection = ForwardArrow->GetForwardVector(); //Need to be normalized?
		float DotDirection = FVector::DotProduct(PawnDirection, ForwardCheckpointDirection);

		if (DotDirection > 0.f)
		{
			Deactivate();
			CheckpointPassed.Broadcast(CheckPointIndex);
		}

	}
}

