// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "MegaCarAttackTamarroGameMode.generated.h"

UCLASS(minimalapi)
class AMegaCarAttackTamarroGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMegaCarAttackTamarroGameMode();
};



