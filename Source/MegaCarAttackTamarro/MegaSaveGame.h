// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SaveGame.h"
#include "MegaSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class MEGACARATTACKTAMARRO_API UMegaSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:

	UPROPERTY(VisibleAnywhere, Category = Savings)
	float SavedBestRaceTime;
	
	UPROPERTY(VisibleAnywhere, Category = Savings)
	float SavedBestLapTime;
	
	UMegaSaveGame();

};
