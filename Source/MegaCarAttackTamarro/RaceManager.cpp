// Fill out your copyright notice in the Description page of Project Settings.

#include "MegaCarAttackTamarro.h"
#include "RaceManager.h"
#include "Checkpoint.h"
#include "MegaPlayerController.h"
#include "Array.h"
#include "EngineUtils.h"

ARaceManager::ARaceManager()
{
	Billboard = CreateDefaultSubobject<UBillboardComponent>(TEXT("Billboard"));
}

void ARaceManager::BeginPlay()
{
	Super::BeginPlay();

	PlayerController = Cast<AMegaPlayerController>(GetWorld()->GetFirstPlayerController());
	SetupPlayerController();
	StartSequence();
}

void ARaceManager::SetupPlayerController()
{
	if (PlayerController)
	{
		PlayerController->MaxLaps = this->MaxLaps;
		PlayerController->GoldTime = this->GoldTime;
		PlayerController->SilverTime = this->SilverTime;
		PlayerController->BronzeTime = this->BronzeTime;

		PlayerController->DefaultBestRaceTime = this->DefaultBestRaceTime;
		PlayerController->DefaultBestLapTime = this->DefaultBestLapTime;

		PlayerController->MapName = this->MapName;
		PlayerController->SaveGameName = this->SaveGameName;
		PlayerController->Setup();
	}

}

void ARaceManager::StartSequence()
{
	NumCheckpoints = Checkpoints.Num();
	if (NumCheckpoints > 0)
	{
		CheckpointsPresentSetup();
	}
	else
	{
		CheckpointsToRetriveSetup();
		NumCheckpoints = Checkpoints.Num();
	}

	if (NumCheckpoints > 0)
	{
		Checkpoints[0]->Activate();
	}

}

void ARaceManager::RestartGame()
{
	UE_LOG(LogTemp, Warning, TEXT("Restarted!"));
	auto controller = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (controller)
	{
		controller->ConsoleCommand("RestartLevel", true);
	}
}

void ARaceManager::LapCheck(int32 CheckpointPassed)
{
	if (CheckpointPassed == NumCheckpoints - 1)
	{
		LapComplete();
	}
	else
	{
		PrepareNextCheckpoint(CheckpointPassed);
	}
	UpdateRespawnLocation(CheckpointPassed);
}

void ARaceManager::LapComplete()
{
	if (!PlayerController->IsRaceComplete())
	{
		if (NumCheckpoints > 0)
		{
			Checkpoints[0]->Activate();
		}
	}
	else
	{
		FTimerHandle RestartTimerHandle;
		GetWorld()->GetTimerManager().SetTimer(RestartTimerHandle, this, &ARaceManager::RestartGame, 2.f, false);
	}
	PlayerController->UpdateLap();
}

void ARaceManager::PrepareNextCheckpoint(int32 CheckpointPassed)
{
	if (NumCheckpoints > 0) 
	{
		Checkpoints[CheckpointPassed + 1]->Activate();
	}
}

void ARaceManager::UpdateRespawnLocation(int32 CheckpointPassed)
{
	if (NumCheckpoints > 0 && PlayerController)
	{
		PlayerController->RespawnLocation = Checkpoints[CheckpointPassed]->GetActorTransform();
	}
}

void ARaceManager::CheckpointsPresentSetup()
{
	for (int CheckpointIterationIndex = 0; CheckpointIterationIndex < NumCheckpoints; ++CheckpointIterationIndex)
	{
		ACheckpoint* tCheckpoint = Checkpoints[CheckpointIterationIndex];
		if (tCheckpoint)
		{
			tCheckpoint->SetCheckpointIndex(CheckpointIterationIndex);
			tCheckpoint->Deactivate();
			tCheckpoint->CheckpointPassed.AddDynamic(this, &ARaceManager::LapCheck);
		}
	}

}

void ARaceManager::CheckpointsToRetriveSetup()
{
	int i = 0;
	UPROPERTY()
	TArray<ACheckpoint*> TempCheckpoint;

	for (TActorIterator<ACheckpoint> CheckpointsIterator(GetWorld()); CheckpointsIterator; ++CheckpointsIterator)
	{
		TempCheckpoint.Insert(*CheckpointsIterator, i);
		++i;
	}
	
	Checkpoints.SetNum(TempCheckpoint.Num());

	for (i = 0; i < Checkpoints.Num(); ++i)
	{
		int RequestedIndex = ((TempCheckpoint[i]->GetCheckpointIndex() - 1) + TempCheckpoint.Num()) % TempCheckpoint.Num();
		Checkpoints[RequestedIndex] = TempCheckpoint[i];
		Checkpoints[RequestedIndex]->SetCheckpointIndex(RequestedIndex);
		Checkpoints[RequestedIndex]->Deactivate();
		Checkpoints[RequestedIndex]->CheckpointPassed.AddDynamic(this, &ARaceManager::LapCheck);

	}

}

