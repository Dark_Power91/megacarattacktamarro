// Fill out your copyright notice in the Description page of Project Settings.

#include "MegaCarAttackTamarro.h"
#include "Engine.h"
#include "Kismet/GameplayStatics.h"
#include "MegaSaveGame.h"
#include "MegaPlayerController.h"
#include "MegaVehicle.h"
#include "MegacarattackTamarroHud.h"
#include "Blueprint/UserWidget.h"
#include "Components/TimelineComponent.h"

#pragma  optimize("",off)

AMegaPlayerController::AMegaPlayerController()
{
	PrimaryActorTick.bCanEverTick = true;
	
	RaceTime = CreateDefaultSubobject<UTimelineComponent>(TEXT("Race timeline"));
	LapTime = CreateDefaultSubobject<UTimelineComponent>(TEXT("Lap timeline"));
}

void AMegaPlayerController::BeginPlay()
{
	Super::BeginPlay();
	Vehicle = Cast<AMegaVehicle>(GetPawn());
	if (Vehicle)
	{
		RespawnLocation = Vehicle->GetTransform();
		Possess(Vehicle);
	}

	//Add widget to Viewport
	if (HUDWidgetClass)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidgetClass);
		if (CurrentWidget)
		{
			CurrentWidget->AddToViewport();
		}
	}

	//Setup Timelines
	if (CurveFloat)
	{
		FOnTimelineFloat RaceProgressFunction;

		RaceProgressFunction.BindUFunction(this, FName("HandleRaceTimeProgress"));
		RaceTime->AddInterpFloat(CurveFloat, RaceProgressFunction);
		RaceTime->SetLooping(true);
		RaceTime->SetIgnoreTimeDilation(true);

		FOnTimelineFloat LapProgressFunction;

		LapProgressFunction.BindUFunction(this, FName("HandleLapTimeProgress"));
		LapTime->AddInterpFloat(CurveFloat, LapProgressFunction);
		LapTime->SetLooping(true);
		LapTime->SetIgnoreTimeDilation(true);
		
	}

}

void AMegaPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMegaPlayerController::RespawnVehicle_Implementation()
{
	FActorSpawnParameters SpawnParams;
	AMegaVehicle* NewVehicle = GetWorld()->SpawnActor<AMegaVehicle>(AMegaVehicle::StaticClass(), RespawnLocation.GetLocation(), RespawnLocation.Rotator(),SpawnParams);
	
	if (!NewVehicle)
	{
		FVector DesiredLocation = RespawnLocation.GetLocation();
		RespawnLocation.SetLocation(DesiredLocation + FVector(0.f, 0.f, 150.f));
		NewVehicle = GetWorld()->SpawnActor<AMegaVehicle>(AMegaVehicle::StaticClass(), RespawnLocation.GetLocation(), RespawnLocation.Rotator(), SpawnParams);
		RespawnLocation.SetLocation(DesiredLocation);
	}

	if (NewVehicle)
	{
		NewVehicle->SetActorScale3D(FVector(2.f, 2.f, 2.f));
		Possess(NewVehicle);
		Vehicle = NewVehicle;
	}
}

void AMegaPlayerController::Restart_Implementation()
{
	bRaceStarted = false;
	GetWorld()->Exec(GetWorld(), TEXT("RestartLevel"));
}

bool AMegaPlayerController::IsRaceComplete()
{
	bRaceComplete = CurrentLap >= MaxLaps;
	return bRaceComplete;
}

void AMegaPlayerController::UpdateLap_Implementation()
{
	if (!bRaceComplete)
	{
		CurrentLap++;

		if (CurrentLapTime < BestLapTime)
		{
			SaveLapTime();
		}

		RefreshHUD();
		LapTime->PlayFromStart();
	}
	else
	{
		if (CurrentRaceTime < BestRaceTime)
		{
			SaveRaceTime();
		}

		if (CurrentLapTime < BestLapTime)
		{
			SaveLapTime();
		}

		RefreshHUD();

		LapTime->Stop();
		RaceTime->Stop();
	}
}

void AMegaPlayerController::Setup_Implementation()
{
	SaveGameCheck();

	CurrentLap = 1;
	if (Vehicle)
	{
		RespawnLocation = Vehicle->GetActorTransform();
	}
	RefreshHUD();
	RaceTime->PlayFromStart();
	LapTime->PlayFromStart();

}

void AMegaPlayerController::ResetSaveing()
{
	BestRaceTime = DefaultBestRaceTime;
	BestLapTime = DefaultBestLapTime;
	SaveChenges();
	RefreshHUD();
}

void AMegaPlayerController::RefreshHUD()
{
	CurrentLapText = FText::AsNumber(CurrentLap);
	MaxLapText = FText::AsNumber(MaxLaps);

	GoldTimeText = TimeConvertion(GoldTime);
	SilverTimeText = TimeConvertion(SilverTime);
	BronzeTimeText = TimeConvertion(BronzeTime);

	BestRaceTimeText = TimeConvertion(BestRaceTime);
	BestLapTimeText = TimeConvertion(BestLapTime);

	CurrentLaptimeText = TimeConvertion(CurrentLapTime);;
	CurrentRaceTimeText = TimeConvertion(CurrentRaceTime);
	
}

void AMegaPlayerController::StartRaceTime()
{
	RaceTime->PlayFromStart();
	//RaceTime->Stop();
}

FText AMegaPlayerController::TimeConvertion(float time)
{
	FString TempTime;
	int mins = time / 60;
	int secs = FMath::Fmod(time, 60);
	int millisecs = (time - FMath::FloorToInt(time)) * 100;

	if (mins < 10)
	{
		TempTime.AppendInt(0);
	}
	TempTime.AppendInt(mins);
	TempTime.Append(":");

	if (secs < 10)
	{
		TempTime.AppendInt(0);
	}
	TempTime.AppendInt(secs);
	TempTime.Append(":");

	if (millisecs < 10)
	{
		TempTime.AppendInt(0);
	}
	TempTime.AppendInt(millisecs);

	return FText::FromString(TempTime);

}

void AMegaPlayerController::HandleRaceTimeProgress(float Value)
{
	CurrentRaceTime = Value;
	RefreshHUD();
}

void AMegaPlayerController::HandleLapTimeProgress(float Value)
{
	CurrentLapTime = Value;
	RefreshHUD();
	
}


void AMegaPlayerController::SaveGameCheck()
{
	UGameplayStatics::DoesSaveGameExist(SaveGameName,0) ? LoadSavedData() : SaveData();
}

void AMegaPlayerController::LoadSavedData()
{
	UMegaSaveGame* tempSave = Cast<UMegaSaveGame>(UGameplayStatics::LoadGameFromSlot(SaveGameName, 0));
	BestLapTime = tempSave->SavedBestLapTime;
	BestRaceTime = tempSave->SavedBestRaceTime;
}

void AMegaPlayerController::SaveData()
{
	BestLapTime = DefaultBestLapTime;
	BestRaceTime = DefaultBestRaceTime;

	SaveGameReference = Cast<UMegaSaveGame>(UGameplayStatics::CreateSaveGameObject(UMegaSaveGame::StaticClass()));

	SaveGameReference->SavedBestLapTime = BestLapTime;
	SaveGameReference->SavedBestRaceTime = BestRaceTime;

	UGameplayStatics::SaveGameToSlot(SaveGameReference, SaveGameName, 0);
}

void AMegaPlayerController::SaveRaceTime()
{
	BestRaceTime = CurrentRaceTime;
	SaveChenges();

}

void AMegaPlayerController::SaveLapTime()
{

	BestLapTime = CurrentLapTime;
	SaveChenges();
}

void AMegaPlayerController::SaveChenges()
{
	SaveGameReference = Cast<UMegaSaveGame>(UGameplayStatics::CreateSaveGameObject(UMegaSaveGame::StaticClass()));

	SaveGameReference->SavedBestLapTime = BestLapTime;
	SaveGameReference->SavedBestRaceTime = BestRaceTime;
	UGameplayStatics::SaveGameToSlot(SaveGameReference, SaveGameName, 0);
}
