// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "VehicleWheel.h"
#include "MegaCarAttackTamarroWheelRear.generated.h"

UCLASS()
class UMegaCarAttackTamarroWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UMegaCarAttackTamarroWheelRear();
};



