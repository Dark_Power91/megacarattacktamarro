// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MegaCarAttackTamarroPawn.h"
#include "MegaVehicle.generated.h"

class AMegaPlayerController;

UCLASS(BlueprintType, Blueprintable)
class MEGACARATTACKTAMARRO_API AMegaVehicle : public AMegaCarAttackTamarroPawn
{
	GENERATED_BODY()

	float RespawnTimer = 0.f;

public:
	AMegaVehicle();

protected:
	UPROPERTY(BlueprintReadOnly)
	AMegaPlayerController* PlayerController;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Respawn ,meta = (BlueprintProtected = "true"))
	float RespawnDelay = 2.f; 

	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

private:
	UFUNCTION()
	void VehicleDestroyed(AActor* DestroyedActor);

	UFUNCTION(BlueprintCallable, Category = Respawn)
	bool RespawnCheck();

	void RespawnRequest();
	void CancelSavingReqeust();

};
