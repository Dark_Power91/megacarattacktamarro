// Fill out your copyright notice in the Description page of Project Settings.

#include "MegaCarAttackTamarro.h"
#include "MegaVehicle.h"
#include"MegaPlayerController.h"
#include "WheeledVehicleMovementComponent.h"


AMegaVehicle::AMegaVehicle()
{
	PrimaryActorTick.bCanEverTick = true;
	AutoPossessPlayer = EAutoReceiveInput::Player0;
	this->OnDestroyed.AddDynamic(this, &AMegaVehicle::VehicleDestroyed);
}

void AMegaVehicle::BeginPlay()
{
	Super::BeginPlay();
	PlayerController =	Cast<AMegaPlayerController>(GetWorld()->GetFirstPlayerController());

}

void AMegaVehicle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (RespawnCheck())
	{
		if (RespawnTimer >= RespawnDelay)
		{
			RespawnTimer = 0.f;
			Destroy();
		}
		else
		{
			RespawnTimer += DeltaTime;
		}

	}

}

void AMegaVehicle::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Respawn", IE_Pressed, this, &AMegaVehicle::RespawnRequest);
	PlayerInputComponent->BindAction("Obliterate", IE_Pressed,this, &AMegaVehicle::CancelSavingReqeust);
	
}

void AMegaVehicle::VehicleDestroyed(AActor* DestroyedActor)
{
	if (PlayerController)
	{
		PlayerController->RespawnVehicle();
	}
}

bool AMegaVehicle::RespawnCheck()
{
	float UpDirection = FVector::DotProduct(GetActorUpVector(), FVector::UpVector);

	return UpDirection < 0.5 && GetVehicleMovement()->GetForwardSpeed() < 1;
	
}

void AMegaVehicle::RespawnRequest()
{
	Destroy();
}

void AMegaVehicle::CancelSavingReqeust()
{
	AMegaPlayerController* controller = Cast<AMegaPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if (controller)
	{
		controller->ResetSaveing();
	}
}
