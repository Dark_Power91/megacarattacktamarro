// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
 //Forward declaration non funziona 
#include "MegaPlayerController.generated.h"

UCLASS(BlueprintType, Blueprintable)
class MEGACARATTACKTAMARRO_API AMegaPlayerController : public APlayerController
{
	GENERATED_BODY()
	
	UPROPERTY(Category = "Race Text",  BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FText CurrentRaceTimeText;
	UPROPERTY(Category = "Race Text", BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FText CurrentLaptimeText;
	UPROPERTY(Category = "Race Text", BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FText CurrentLapText;
	UPROPERTY(Category = "Race Text", BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FText GoldTimeText;
	UPROPERTY(Category = "Race Text", BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FText SilverTimeText;
	UPROPERTY(Category = "Race Text", BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FText BronzeTimeText;
	UPROPERTY(Category = "Race Text", BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FText MaxLapText;
	UPROPERTY(Category = "Race Text", BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FText BestRaceTimeText;
	UPROPERTY(Category = "Race Text", BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FText BestLapTimeText;



	UPROPERTY(Category = "Race State",  BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	bool bRaceComplete = false;
	UPROPERTY(Category = "Race State",  BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	bool bRaceStarted = false;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class AMegaVehicle* Vehicle;

	UPROPERTY()
	class UTimelineComponent* RaceTime;
	UPROPERTY()
	class UTimelineComponent* LapTime;
	class UMegaSaveGame* SaveGameReference;

public:

	UPROPERTY(Category = Values, VisibleAnywhere, BlueprintReadOnly)
	int CurrentLap;
	UPROPERTY(Category = Values, VisibleAnywhere, BlueprintReadOnly)
	int MaxLaps;
	UPROPERTY(Category = "Race State", EditAnywhere, BlueprintReadOnly)
	float CurrentRaceTime;
	UPROPERTY(Category = "Race State", EditAnywhere, BlueprintReadOnly)
	float CurrentLapTime;
	UPROPERTY(Category = "Race State", EditAnywhere, BlueprintReadOnly)
	float GoldTime;
	UPROPERTY(Category = "Race State", EditAnywhere, BlueprintReadOnly)
	float SilverTime;
	UPROPERTY(Category = "Race State", EditAnywhere, BlueprintReadOnly)
	float BronzeTime;
	UPROPERTY(Category = "Race State", EditAnywhere, BlueprintReadOnly)
	float BestRaceTime;
	UPROPERTY(Category = "Race State", EditAnywhere, BlueprintReadOnly)
	float BestLapTime;
	UPROPERTY(Category = "Race State", EditAnywhere, BlueprintReadOnly)
	float DefaultBestRaceTime;
	UPROPERTY(Category = "Race State", EditAnywhere, BlueprintReadOnly)
	float DefaultBestLapTime;

	UPROPERTY(Category = Saving, BlueprintReadOnly)
	FText MapName;
	UPROPERTY(Category = Saving, BlueprintReadOnly)
	FString SaveGameName;


	UPROPERTY(Category = Respawn, BlueprintReadWrite)
	FTransform RespawnLocation;

	UPROPERTY(EditAnywhere, Category = "Timeline")
	class UCurveFloat* CurveFloat;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Widget", meta = (BlueprintProtected = "true"))
	TSubclassOf<class UUserWidget> HUDWidgetClass;


	
	UPROPERTY()
	class UUserWidget* CurrentWidget;

public:
	AMegaPlayerController();

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
	void RespawnVehicle();
	void RespawnVehicle_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	void Restart();
	void Restart_Implementation();

	UFUNCTION()
	bool IsRaceComplete();

	UFUNCTION(BlueprintNativeEvent)
	void UpdateLap();
	void UpdateLap_Implementation();

	UFUNCTION(BlueprintNativeEvent)
	void Setup();
	void Setup_Implementation();
	
	UFUNCTION()
	void ResetSaveing();

private:
	void RefreshHUD();
	void StartRaceTime();

	FText TimeConvertion(float time);

	UFUNCTION()
	void HandleRaceTimeProgress(float Value);

	UFUNCTION()
	void HandleLapTimeProgress(float Value);

	void SaveGameCheck();
	void LoadSavedData();
	void SaveData();

	void SaveRaceTime();
	void SaveLapTime();
	void SaveChenges();


};
