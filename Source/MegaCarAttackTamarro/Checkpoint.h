// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Checkpoint.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCheckpointPassed, int32, CheckpointIndex);

UCLASS(BlueprintType, Blueprintable)
class MEGACARATTACKTAMARRO_API ACheckpoint : public AActor
{
	GENERATED_BODY()

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	USceneComponent* Root;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UArrowComponent* ForwardArrow;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* CollisionBox;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UParticleSystemComponent* PassageEffect;

public:
	UPROPERTY(BlueprintCallable, Category = Delegates)
	FOnCheckpointPassed CheckpointPassed;

	// Sets default values for this actor's properties
	ACheckpoint();

protected:
	UPROPERTY(BlueprintReadOnly, Category = Values)
	int32 CheckPointIndex;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	

	UFUNCTION(BlueprintCallable, Category = Checkpoint)
	void Activate();

	UFUNCTION(BlueprintCallable, Category = Checkpoint)
	void Deactivate();

	UFUNCTION(BlueprintPure, Category = Checkpoint)
	int32 GetCheckpointIndex();

	UFUNCTION(BlueprintCallable, Category = Checkpoint)
	void SetCheckpointIndex(int32 Index);

	
private:
	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
